<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['middleware' => 'auth'], function () {
	/** View Tweets **/
	Route::get('Twitter', 'Twitter@index');
	Route::get('Twitter/{hash}', 'Twitter@tweets');
	Route::get('Twitter/Popular/{hash}', 'Twitter@popularTweets');

	Route::get('Twitter/Delete/{hash}', 'Twitter@delete');


	/** Manage Tags **/
	Route::get('Hash', 'HashMonitor@index');
	Route::get('Hash/getData', 'HashMonitor@getData');
	
	Route::post('Hash', 'HashMonitor@insert');

	Route::post('Hash/Delete', 'HashMonitor@delete');
});
