<?php

use Illuminate\Database\Seeder;

/**
 * Class AdminUserSeeder
 */
class AdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        factory(App\User::class)->create([
                "name" => "Admin",
                "email" => "admin@example.com",
                "password" => bcrypt(env('ADMIN_PWD', '123456'))]
        );
    }
}