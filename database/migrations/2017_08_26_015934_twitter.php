<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Twitter extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        
        Schema::create('twitter', function (Blueprint $table) {
            $table->increments('id');
            $table->bigInteger('twitter_id')->unique();
            $table->bigInteger('created_tweet_at_epoch');
            $table->string('created_tweet_at');
            $table->string('hash');
            $table->string('name');
            $table->string('screen_name');
            $table->string('location')->nullable();
            $table->string('text');
            $table->string('source');
            $table->bigInteger('retweet_count');
            $table->timestamps();
        });
        Schema::create('hashMonitor', function (Blueprint $table) {
            $table->increments('id');
            $table->string('hash')->unique();
            $table->integer('time');
            $table->integer('pid')->default(0);
            $table->timestamps();
        });
        //  DB::unprepared('
        //     CREATE EVENT `DELETE_OLD_TWEETS`
        //         ON SCHEDULE
        //             EVERY 5 MINUTE
        //         ON COMPLETION PRESERVE
        //         ENABLE
        //         COMMENT ""
        //     DO BEGIN
        //         DELETE FROM 
        //            twitter 
        //         WHERE 
        //            created_tweet_at_epoch < UNIX_TIMESTAMP(NOW() - INTERVAL 12 HOUR);
        //     END
        // ');
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('twitter');
        Schema::dropIfExists('hashMonitor');
        // DB::unprepared('DROP EVENT IF EXISTS DELETE_OLD_TWEETS');
    }
}
