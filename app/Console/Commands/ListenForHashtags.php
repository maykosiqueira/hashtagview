<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use TwitterStreamingApi;
use App\twitter AS TwitterModel;
use App\hashMonitor AS HashMonitorModel;

class ListenForHashtags extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'ListenForHashtags {hash}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Listen for hashtags being used on Twitter in Real-Time';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $hash = $this->argument('hash');
        TwitterStreamingApi::publicStream()
            ->whenHears("#{$hash}", function (array $tweet) {
                $hash = $this->argument('hash');
                //Kill command if is expired or not registred
                if (HashMonitorModel::isExpired($hash)) {
                    exit('Expired Monitor');
                }
                //Check if tweet has been inserted
                if (isset($tweet['id']) && !TwitterModel::where('twitter_id', $tweet['id'])->count()) {
                    $location = '';
                    if (!empty($tweet['user']['location']))
                        $location = $tweet['user']['location'];
                    $time = strtotime($tweet['created_at']);

                    $obj = new TwitterModel();

                    $obj->twitter_id = $tweet['id'];
                    $obj->created_tweet_at_epoch = $time;
                    $obj->created_tweet_at = $tweet['created_at'];
                    $obj->hash = $hash;
                    $obj->name = $tweet['user']['name'];
                    $obj->screen_name = $tweet['user']['screen_name'];
                    $obj->location = $location;
                    $obj->text = $tweet['text'];
                    $obj->source = $tweet['source'];
                    $obj->retweet_count = $tweet['retweet_count'];

                    $obj->save();
                }

            })
            ->startListening();
    }
}
