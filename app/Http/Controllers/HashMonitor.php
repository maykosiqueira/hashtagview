<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\hashMonitor AS HashMonitorModel;
use App\twitter AS TwitterModel;
use Symfony\Component\Process\Process;

class HashMonitor extends Controller
{
    public function index() {
    	return view('HashMonitor');
    }

    public function getData() {
    	$data = HashMonitorModel::all();
    	$retData = [];

    	foreach ($data as $key => $value) {
    		$timestampCreated = strtotime($value->created_at);
			$retData[] = [
				'id' => $value->id,
				'hash' => $value->hash,
				'created_at' => $timestampCreated*1000,
				'time' => $value->time,
				'duration' => time() - $timestampCreated,
				'tweets_created' => TwitterModel::where([
						['hash', '=' ,$value->hash],
						['created_tweet_at_epoch','>=',$timestampCreated]
					])->count(),
                'tweets_hashtag' =>  TwitterModel::where([
                        ['hash', '=' ,$value->hash],
                    ])->count(),

			];
    	}

    	return $retData;

    }

    public function delete(Request $request) {
        $params = $request->all();
        $id = $params['id'];
        $deleteTweets = $params['deleteTweets'];


        //Find Hash Monitor
        $monitor = HashMonitorModel::find($id);

        //Get PID to kill
        $pid = $monitor->pid + 1;

        //Get hash name
        $hash = $monitor->hash;

        //Exclude tweets with hash
        if ($deleteTweets) {
            TwitterModel::where('hash', $hash)->delete();
        }

        //Delete hash
        $monitor->delete();

        //Kill Process
        $process = new Process("kill {$pid}");
        $process->disableOutput();
        $process->run();

        return [
            'success' => "Monitoramento da hash {$hash} parado."
        ];
    }

    public function insert(Request $request) {
    	$params = $request->all();
        try {
            DB::beginTransaction();
        	$hasHash = HashMonitorModel::where('hash',$params['hash']);

        	if ($hasHash->count())
        		return [
        			'error' => 'Hashtag já está cadastrada.'
        		];

        	$monitor = new HashMonitorModel();
        	$monitor->hash = $params['hash'];
        	$monitor->time = $params['time'];
        	$monitor->save();

            //Dispatch listner command
            $process = new Process("nohup php /home/maykon/public_html/hashRealTimeTwitter/artisan ListenForHashtags {$params['hash']}  >> /home/maykon/public_html/hashRealTimeTwitter/hash.txt 2>> /home/maykon/public_html/hashRealTimeTwitter/error.txt &");
            $process->start();

            //Save PID
            $monitor->pid = $process->getPid();
            $monitor->save();

            DB::commit();
        	return [ 'success' => 'Salvo com sucesso.'];
        } catch (Exception $e) {
            DB::rollBack();
            return ['error' => $e->getMessage()];
        }
    }
}
