<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use TwitterApi;
use App\twitter AS TwitterModel;

class Twitter extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index() {
        return view('Twitter');
    }

    /**
     * Show the tweets.
     *
     * @return Response
     */
    public function tweets($hash)
    {
        TwitterModel::updateHash($hash);
        return TwitterModel::where('hash', $hash)->orderBy('id', 'desc')->get();
    }


    /**
     * Delete the tweets.
     *
     * @return Response
     */
    public function delete($hash)
    {
        TwitterModel::where('hash', $hash)->delete();
        return [
            'success' => 'Excluido com sucesso.'
        ];
    }


    /**
     * Show the tweets.
     *
     * @return Response
     */
    public function tweetsNew($hash, $first)
    {
        return TwitterModel::where([
                ['hash','=', $hash],
                ['id','>', $first],
            ])->orderBy('id', 'desc')->get();
    }

    /**
     * Show popular tweets.
     *
     * @return Response
     */
    public function popularTweets($hash) {
        //Params to search
        $params = [
            'q' => "#{$hash}",
            'result_type' => 'popular',
            'count' => 3
        ];
        
        $result = TwitterApi::getSearch($params);

        return $result->statuses;
    }
}
