<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\hashMonitor AS HashMonitorModel;
use TwitterStreamingApi;
use TwitterApi;

class twitter extends Model
{
	//Table name
	protected  $table = 'twitter';

    //Select last tweet with hash
    private static function getLastTweet($hash) {
        return self::where('hash', $hash)->orderBy('id', 'desc')->first();
    }
    //Select first tweet with hash
    private static function getFirstTweet($hash) {
        return self::where('hash', $hash)->orderBy('id', 'asc')->first();
    }

    public static function updateHash($hash) {
        //If not mining all initial results, rework
        $first = self::getFirstTweet($hash);

        //Get time limit
        $monitor = HashMonitorModel::where('hash', $hash);
        if ($monitor->count()) {
            $monitor = $monitor->first();
            $limitTime = strtotime("-{$monitor->time} hour UTC");
        } else {
            $limitTime = strtotime('-12 hour UTC');
        }

        if ($first && $first->created_tweet_at_epoch > $limitTime ) {
            $results = self::search($hash,0,$first->twitter_id);
            self::saveResults($results, $hash);
        } else if (!$first) {
             //Update lastest results
            $last = self::getLastTweet($hash);
            if ($last) {
                $results = self::search($hash,0,$last->twitter_id);
            } else {
                $results = self::search($hash);
            }
            self::saveResults($results, $hash);
        }
    }

    private static function saveResults($results, $hash) {
    	$insert = [];
		$timeAgo = strtotime('-12 hour UTC');
    	foreach ($results as $key => $tweet) {
    		$location = '';
    		if (!empty($tweet->user->location))
    			$location = $tweet->user->location;
    		$time = strtotime($tweet->created_at);
    		if ($time > $timeAgo) {
	    		$obj = new twitter();
	    		$obj->twitter_id = $tweet->id;
	    		$obj->created_tweet_at_epoch = $time;
	    		$obj->created_tweet_at = $tweet->created_at;
	    		$obj->hash = $hash;
	    		$obj->name = $tweet->user->name;
	    		$obj->screen_name = $tweet->user->screen_name;
	    		$obj->location = $location;
	    		$obj->text = $tweet->text;
	    		$obj->source = $tweet->source;
	    		$obj->retweet_count = $tweet->retweet_count;

	    		$insert[] = $obj->attributesToArray();
    		}
    	}

    	twitter::insertIgnore($insert);
    }

    private static function search($hash, $maxId = 0, $sincId = 0, $depth = 0) {
	    //Max depth search
	    $maxDepth = 50;
    	$timeAgo = strtotime('-12 hour UTC');

        //Params to search
        $params = [
            'q' => "#{$hash}",
            'result_type' => 'recent',
            'count' => 100
        ];

        //Offset
        if ($maxId) {
            $params = array_merge($params, ['max_id' => $maxId]);
        }

        try {
            $result = TwitterApi::getSearch($params);
        } catch (Exception $e) {
            return [];
        }

        $results = [];

        //Check if more result are available
        if (isset($result->search_metadata->next_results)) {
            //Get next result metadata
            $nextResult  = $result->search_metadata->next_results;

            //Get next offset
            $max_id = explode('&',explode('=', $nextResult)[1]);

            //Get last result to check time
            $length = count($result->statuses);
            $lastResult = $result->statuses[$length - 1];
            $lastTime = strtotime($lastResult->created_at);

            //Check if max depth search reached and the creation time is less than 12 hours
            if ($max_id && $depth < $maxDepth && $lastTime > $timeAgo && $lastResult->id > $sincId) {
                //If not, search next offset
                $results = array_merge_recursive((array) $result->statuses, self::search($hash, $max_id, $sincId, ++$depth));
            } else {
                $results = (array) $result->statuses;
            }
        }

        return $results;

    }
}
