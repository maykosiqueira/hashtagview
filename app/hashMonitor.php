<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class hashMonitor extends Model
{
	//Table name
	protected  $table = 'hashMonitor';

	/*
	 * Check if hash is expired
	 */
	public static function isExpired($hash) {
		$monitor = self::where('hash', $hash);
		if (!$monitor->count())
			return -1;

		$monitor = $monitor->first();

		$currentTimestamp = time();
		$createTimeStamp = strtotime($monitor->created_at);
		$target = $monitor->time * 60 * 60;//Time in seconds

		if ($currentTimestamp - $createTimeStamp > $target) {
			$monitor->delete();
			return 1;
		} else {
			return 0;
		}
	}
}
