@extends('adminlte::page')

@section('htmlheader_title')
    @lang('twitter.pageTitle')
@endsection

@section('contentheader_title')
	@lang('twitter.pageTitle')
@endsection

@section('cssFiles')
    <link href="{{ asset('/plugins/jquery.dataTables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/plugins/dataTables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('/css/custom.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('jsFiles')
    <script src="{{asset('/plugins/jquery.dataTables.min.js') }}" type="text/javascript"></script>
    <script src="{{asset('/plugins/dataTables.bootstrap.min.js') }}" type="text/javascript"></script>
    <script src="{{asset('/plugins/moment.min.js') }}" type="text/javascript"></script>
    <script src="{{asset('/plugins/jquery.validate-json.js') }}" type="text/javascript"></script>
    <script src="{{asset('/plugins/chartJs/Chart.min.js') }}" type="text/javascript"></script>
    <script src="{{asset('/plugins/chartJs/Chart.bundle.min.js') }}" type="text/javascript"></script>
    <script src="{{asset('/js/global.js') }}" type="text/javascript"></script>
    <script src="{{asset('/js/twitter/twitter.js') }}" type="text/javascript"></script>
@endsection


@section('main-content')
	<div class="container-fluid spark-screen">
		<div class="row">
            <div class="box box-info">
                <div class="box-header with-border">
                    <h3 class="box-title">Busca</h3>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-3">
                            <label>Hastag</label>
                        </div>
                    </div>
                    <div class="row" style="padding-bottom: 5px;">
                        <div class="col-md-3">
                            #<input id="hash" type="text" style="width: 95%; padding: 6px 12px;">
                        </div>
                        <div class="col-md-1">
                            <input id="search" type="button" class="btn btn-success" value=@lang('twitter.view')>
                        </div>
                        <div class="col-md-1">
                            <input id="clear" type="button" class="btn btn-danger" value=@lang('twitter.clear')>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
			<div class="box box-warning">
                <div class="box-header with-border">
                    <h3 class="box-title">Tweets</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id='twitterTable' class="table">
                                <thead>
                                    <th>@lang('twitter.user')</th>
                                    <th>@lang('twitter.name')</th>
                                    <th>Tweet</th>
                                    <th>@lang('twitter.date')</th>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
			</div>
            <!-- /.box -->

            <div class="box box-success">
                <div class="box-header with-border">
                    <h3 class="box-title">Popular</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table id='twitterPopular' class="table">
                                <thead>
                                    <th>@lang('twitter.user')</th>
                                    <th>@lang('twitter.name')</th>
                                    <th>Tweet</th>
                                    <th>@lang('twitter.date')</th>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->

            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title">Gráficos</h3>
                    <div class="box-tools pull-right">
                        <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Minimizar">
                            <i class="fa fa-minus"></i></button>
                    </div>
                </div>
                <!-- /.box-header -->
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="row">
                                <div class="col-md-8">
                                    <canvas id="platform"></canvas>
                                </div>
                                <div class="col-md-4">
                                    <div id="platformLegend">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                        </div>
                    </div>
                </div>
                <!-- /.box-body -->
            </div>
            <!-- /.box -->
		</div>
	</div>

    <section id="messages">
        <div id="errorMessage" class="callout callout-danger" style="display: none;"> 
            <button id="errorClose" type="button" class="close" data-dismiss="danger" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <p id="errorContent"></p>
        </div>
        <div id="warningMessage" class="callout callout-warning" style="display: none;">
            <button id="warningClose" type="button" class="close" data-dismiss="warning" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <p id="warningContent"></p>
        </div>
        <div id="successMessage" class="callout callout-success" style="display: none;" >
            <button id="successClose" type="button" class="close" data-dismiss="success" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
            <p id="successContent"></p>
        </div>
        </section>  
    </section>
@endsection
