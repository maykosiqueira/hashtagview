<?php
	return [
		'user' => 'Usuário',
		'name' => 'Nome',
		'pageTitle' => 'Gerenciar Hashtags',
		'view' => 'Visualizar',
		'stop' => 'Parar',
		'created_at' => 'Criado em',
		'date' => 'Data',
		'time' => 'Tempo',
		'duration' => 'Duração',
		'count' => 'Tweets',
		'add' => 'Adicionar',

	];