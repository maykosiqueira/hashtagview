<?php
	return [
		'user' => 'Usuário',
		'name' => 'Nome',
		'pageTitle' => 'Visualizador de Hashtags',
		'view' => 'Visualizar',
		'stop' => 'Parar',
		'clear' => 'Limpar Tweets',
		'date' => 'Data',

	];