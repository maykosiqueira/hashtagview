<?php

use Spatie\Menu\Laravel\Menu;


Menu::macro('adminlteSubmenu', function ($submenuName) {
    return Menu::new()->prepend('<a href="#"><span> ' . $submenuName . '</span> <i class="fa fa-angle-left pull-right"></i></a>')
        ->addParentClass('treeview')->addClass('treeview-menu');
});
Menu::macro('adminlteMenu', function () {
    return Menu::new()
        ->addClass('sidebar-menu');
});
Menu::macro('adminlteSeparator', function ($title) {
    return Html::raw($title)->addParentClass('header');
});

Menu::macro('adminlteDefaultMenu', function ($content) {
    return Html::raw('<i class="fa fa-link"></i><span>' . $content . '</span>')->html();
});

Menu::macro('sidebar', function () {
    return Menu::adminlteMenu()
        ->add(Html::raw('')->addParentClass('header'))
        ->add(Link::toUrl('/Hash', '<i class="fa fa-hashtag"></i><span>Gerenciar Hashs</span>'))
        ->add(Link::toUrl('/Twitter', '<i class="fa fa-twitter"></i><span>Visualizar</span>'));
});
