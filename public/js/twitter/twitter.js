var tableContent;
var tablePopular;
var platformChart;

var platformTweets = {
	'Android' : 0,
	'Iphone' : 0,
	'Desktop' :  0,
	'Facebook' :  0,
	'Instagram' :  0,
	'Twitter_Lite' :  0,
	'Mobile_Web' :  0,
	'TweetDeck': 0,
	'FXS' :  0,
	'Outros' : 0,
};
const chartColors = {
    red:        '#dd4b39',
    green:      '#00a65a',
    yellow:     '#bbda14',
    aqua:       '#00c0ef',
    fuchsia:    '#f012be',
    lime:       '#01ff70',
    orange:     '#ed9212',
    navy:       '#001f3f',
    teal:       '#39cccc',
    gray_light: '#bababa',
    olive:      '#3d9970',
    maroon:     '#d81b60',
    blue:       '#0073b7',
    purple:     '#605ca8',
    gray:       '#a9a9a9',
};
//Array das cores para poder gerar grafico de doughnut de forma padrão
const chartColorsArray = Object.keys(chartColors)
.map(function(k) {
    return  chartColors[k];
});

$(document).ready(function() {
	tableContent = $('#twitterTable').DataTable({
            "language": {
                "url": "/plugins/Portuguese-Brasil.json"
            },
        	"deferRender": true,
        	"colReorder": {
	            "realtime": false
	        },
	        "columnDefs": [
	            {
	                "render": dataTableDateRender,
	                "targets": 3
	            }
	        ]
	});
	tablePopular = $('#twitterPopular').DataTable({
            "language": {
                "url": "/plugins/Portuguese-Brasil.json"
            },
            "dom": 't',
	        "columnDefs": [
	            {
	                "render": dataTableDateRender,
	                "targets": 3
	            }
	        ]
	});
	initializeSearch();
	initializeCharts();
});

function initializeCharts() {
    initilizeDoughnutChart();
    // initializeBarChart();
    // initializeAreaChart();
}

function resetVariables() {
	platformTweets = {
		'Android' : 0,
		'Iphone' : 0,
		'FXS' :  0,
		'Desktop' :  0,
		'Facebook' :  0,
		'Instagram' :  0,
		'Twitter_Lite' :  0,
		'TweetDeck': 0,
		'Mobile_Web' :  0,
		'Outros' : 0,
	};
}

function initializeSearch() {
	$('#search').click(function() {
		var searchVal = $('#hash').val();
		searchVal = searchVal.replace('#','');

		if (!searchVal) {
			errorMessage('Por gentileza informe a tag que deseja buscar.');
			return;
		}

		$('#search').prop('disabled', true);
		$('#hash').prop('disabled', true);

		ajaxSearch();
	})
	$('#hash').keydown(function(e) {
	    if(e.keyCode == 13) {
	        $('#search').click();
	    }
	});
	$('#clear').click(function() {
		var searchVal = $('#hash').val();
		searchVal = searchVal.replace('#','');

		if (!searchVal) {
			errorMessage('Por gentileza informe a tag que deseja limpar.');
			return;
		}

		if (!confirm("Dejesa limpar os dados referente a esta tag?")) 
			return;

		$('#search').prop('disabled', true);
		$('#hash').prop('disabled', true);
		$.ajax({
			url: '/Twitter/Delete/'+hash,
			type: 'GET',
			dataType: 'JSON',
			success: function() {
				successMessage('Dados limpos com sucesso.');
			}, 
			error: function() {
				errorMessage('Um erro inesperado aconteceu.');
			},
			complete: function() {
				$('#search').click();
			}
		})

	})

}

function ajaxSearch() {
	var searchVal = $('#hash').val();
		searchVal = searchVal.replace('#','');

	$.ajax({
		url: 'Twitter/'+searchVal,
		type: 'GET',
		dataType: 'json',
		beforeSend: function() {
			resetVariables();
			tableContent.clear();
		},
		success: function(data) {
			$.each(data, function(key, val) {
				//If no exists timestamp, generate
				if (!val.timestamp_ms) {
					var date = new Date(val.created_tweet_at)
					val.timestamp_ms = moment.utc(date).valueOf();
				}

				countTweetsPlatform(val);
				
				addRow(val);
			});
		},
		error: function(err) {
			$('#stop').click();

			errorMessage('Ocorreu um erro inesperado.');
		},
		complete: function() {
			//Order by timestamp
			tableContent.order([3, 'desc']).draw();
			updateCharts();
		}
	});

	$.ajax({
		url: 'Twitter/Popular/'+searchVal,
		type: 'GET',
		dataType: 'json',
		beforeSend: function() {
			tablePopular.clear();
		},
		success: function(data) {
			$.each(data, function(key, val) {
				//If no exists timestamp, generate
				if (!val.timestamp_ms) {
					var date = new Date(val.created_at)
					val.timestamp_ms = moment.utc(date).valueOf();
				}
				
				addRowPopular(val);
			});
		},
		error: function(err) {
			$('#stop').click();

			errorMessage('Ocorreu um erro inesperado.');
		},
		complete: function() {
			//Order by timestamp
			tablePopular.order([3, 'desc']).draw();
			$('#search').prop('disabled', false);
			$('#hash').prop('disabled', false);
		}
	});
}

function addRow(obj) {
	tableContent.row.add([
		obj.screen_name,
		obj.name,
		obj.text,
		parseInt(obj.timestamp_ms)
	]);
}

function addRowPopular(obj) {
	tablePopular.row.add([
		obj.user.screen_name,
		obj.user.name,
		obj.text,
		parseInt(obj.timestamp_ms)
	]);
}

function countTweetsPlatform(val) {
	//Count access by platform
	if (val.source.indexOf('android') >= 0 || val.source.indexOf('Android') >= 0) {
		platformTweets['Android'] ++;
	} else if (val.source.indexOf('iphone') >= 0) {
		platformTweets['Iphone'] ++;
	} else if (val.source.indexOf('fxspider') >= 0) {
		platformTweets['FXS'] ++;
	} else if (val.source.indexOf('Facebook') >= 0) {
		platformTweets['Facebook'] ++;
	} else if (val.source.indexOf('Instagram') >= 0) {
		platformTweets['Instagram'] ++;
	} else if (val.source.indexOf('Twitter Lite') >= 0) {
		platformTweets['Twitter_Lite'] ++;
	} else if (val.source.indexOf('Mobile Web') >= 0) {
		platformTweets['Mobile_Web'] ++;
	} else if (val.source.indexOf('Web Client') >= 0) {
		platformTweets['Desktop'] ++;
	} else if (val.source.indexOf('TweetDeck') >= 0) {
		platformTweets['TweetDeck'] ++;
	} else {
		platformTweets['Outros'] ++;
	}
}

function updateCharts() {
    var datasets = [];
    var platforms = [];
    var labels = [];
    $.each(platformTweets, function(platform, count) {
        platforms.push(count.toString());
        labels.push(platform.replace('_',' ')); 
    });

    datasets.push(generateDataset(platforms,'',''));

	platformChart.data.datasets = datasets;
    platformChart.data.labels = labels;
	platformChart.update();

	$('#platformLegend').html(platformChart.generateLegend());
}

function initilizeDoughnutChart() {
    var ctx = $("#platform");

    platformChart = new Chart(ctx, {
        type: 'doughnut',
        options: {
            legendCallback: function(chart) {
                var text = [];
                text.push('<ul style="padding: 0px;margin:0px;">');
                for (var i=0; i<chart.data.datasets[0].data.length; i++) {
                    text.push('<li style="list-style-type: none;">');
                    text.push('<span class="fa fa-circle-o" style="color:' + chart.data.datasets[0].backgroundColor[i] + '; margin-right: 5px;"></span>');
                    if (chart.data.labels[i]) {
                        text.push('<i style="font-size:9px;">'+chart.data.labels[i]+'</i>');
                    }
                    text.push('</li>');
                }
                text.push('</ul>');
                return text.join("");
            },
            legend: {
                display: false,
                position: 'right',
            },
        },


    });
}