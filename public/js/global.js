Date.prototype.formatBRT = function() {
    var yyyy = this.getFullYear().toString();
    var MM = (this.getMonth()+1).toString(); // getMonth() is zero-based
    var dd  = this.getDate().toString();
    var hh = this.getHours().toString();
    var mm = this.getMinutes().toString();

    //return yyyy + (MM[1]?MM:"0"+MM[0]) + (dd[1]?dd:"0"+dd[0]); // padding
    return (dd[1]?dd:"0"+dd[0]) + '/' + (MM[1]?MM:"0"+MM[0]) + '/' + yyyy + ' ' + (hh[1]?hh:"0"+hh[0]) + ':'
        + (mm[1]?mm:"0"+mm[0]);
};

Date.prototype.formatUTCDate = function() {
    var day = this.getUTCDate(),
        month = this.getUTCMonth() + 1;
    return (day < 10 ? '0'+day : day) + '/' + (month < 10 ? '0'+month : month) + '/' + this.getUTCFullYear();
};


$(document).ready(function() {
	//Listner do botão fechar
	$('.close').click(function() {
		$(this).parent().slideUp();
	});
});
function warningMessage(msg) {
	message('warning',msg);
}
function successMessage(msg) {
	message('success',msg);
}
function errorMessage(msg) {
	message('error',msg);
}

function message(type, msg) {
	$("#" + type + "Content").html(msg);
	$("#" + type + "Message").slideDown();

	setTimeout(function() {
	   $("#" + type + "Message").slideUp();
	}, 4500);	

}

function epochToLocalString(epoch) {
    if (epoch == '-')
        return epoch;
    var date = new Date(epoch);
    return date.formatBRT();
}

function dataTableDateRender(data, type, row) {
    if(data === 'N/A'){
        return data;
    }
    if (type === 'display' || type === 'filter') {
        return epochToLocalString(data);
    }
    return data;
}

function jsonParse(json) {
	try {
		return [JSON.parse(json)];
	} catch (e) {
		var msg  = e.message.replace(/ /g,'').split('column')[1];
		if (msg && typeof json === "string") {
			var column = parseInt(msg.match(/\d+/)[0])-1;
			var jsons = [];
			try {
				jsons.push(JSON.parse(json.slice(0, column))); 
				var aux = jsonParse(json.slice(column));
				jsons.concat(aux);
				return jsons;
			} catch (e) {
				console.log(json);
				return [];
			}
		} else {
			console.log(json);
			console.log(e);
		}

		return [];
	}
}
function generateDataset(dados, bgColor, legend, alpha, area) {
    if (!alpha) {
        alpha = 1;
    }
    var bg = [];
    if (bgColor) {
        bg = Array(dados.length).fill(hexToRgbA(bgColor, alpha));
    } else {
        for(var i=0;i<dados.length;i++) {
            bg.push(chartColorsArray[i]);
        }
    }
    if (area) {
        bg = bg[0];
    }
    return {
        data: dados,
        backgroundColor: bg,
        label: legend,
    };
}

/*
 https://stackoverflow.com/questions/21646738/convert-hex-to-rgba
 */
function hexToRgbA(hex, alpha){
    var c;
    if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
        c= hex.substring(1).split('');
        if(c.length== 3){
            c= [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c= '0x'+c.join('');
        return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',')+', ' + alpha + ')';
    }
    throw new Error('Bad Hex');
}