var tableContent;
var ajaxObj;
var tweetsBar;

const refreshTime = 1000*5;//5 segundos

const chartColors = {
    red:        '#dd4b39',
    green:      '#00a65a',
    yellow:     '#bbda14',
    aqua:       '#00c0ef',
    fuchsia:    '#f012be',
    lime:       '#01ff70',
    orange:     '#ed9212',
    navy:       '#001f3f',
    teal:       '#39cccc',
    gray_light: '#bababa',
    olive:      '#3d9970',
    maroon:     '#d81b60',
    blue:       '#0073b7',
    purple:     '#605ca8',
    gray:       '#a9a9a9',
};
//Array das cores para poder gerar grafico de doughnut de forma padrão
const chartColorsArray = Object.keys(chartColors)
.map(function(k) {
    return  chartColors[k];
});

$(document).ready(function() {
	tableContent = $('#hashtagsTable').DataTable({
            "language": {
                "url": "/plugins/Portuguese-Brasil.json"
            },
	        "columnDefs": [
	            {
	                "render": dataTableDateRender,
	                "targets": 1
	            }
	        ]
	});

	$('#time').inputmask("99");

	initializeButtons();
	initializeCharts();

	updateHashtags();
	setInterval(updateHashtags, refreshTime);
}).on('click', '.stop', function() {
	var id = $(this).prop('id');
	var deleteTweets = 0;
	var token = $('#_token').val();

	if (!confirm('Deseja parar o monitoramento desta hashtag?')) {
		return;
	}

	var postData = {
		id: id,
		deleteTweets: deleteTweets,
		_token: token,
	}

	$.ajax({
		url: '/Hash/Delete',
		data: postData,
		type: 'POST',
		dataType: 'JSON',
		success: function(data) {
			if (data.success) {
				successMessage(data.success);
				updateHashtags();
			} else {
				errorMessage(data.error);
			}
		},
		error: function(err){
			errorMessage('Um erro inesperado aconteceu.');
		},
	})
});

function initializeCharts() {
    // initilizeDoughnutChart();
    initializeBarChart();
    // initializeAreaChart();
}

function initializeButtons() {
	$('#add').click(function() {
		var tag = $('#hash').val().replace('#','');
		var time = parseInt($('#time').val());
		var token = $('#_token').val();

		if (!tag) {
			errorMessage('Por gentileza informe a Hashtag que deseja monitorar.');
			return;
		}
		if (!time) {
			errorMessage('Por gentileza informe o tempo que deseja monitorar.');
			return;
		}

		var postData = {
			hash: tag,
			time: time,
			_token: token
		};

		ajaxObj = $.ajax({
			url: '/Hash',
			data: postData,
			type: 'POST',
			dataType: 'JSON',
			beforeSend: function() {
				$('#add').prop('disabled', true);
			},
			 xhrFields: {
                onprogress: function(e)
                {
					ajaxObj.abort();
					clearInputs();
					successMessage('Salvo com sucesso	');
					updateHashtags();
                }
            },
			success: function(data) {
				if (data.success) {
					clearInputs();
					successMessage(data.success);
					updateHashtags();
				} else {
					errorMessage(data.error);
				}
			},
			complete: function() {
				$('#add').prop('disabled', false);
			}
		});
	});

	//Submit on press enter
	$('#hash').keydown(function(e) {
	    if(e.keyCode == 13) {
	        $('#add').click();
	    }
	});
	$('#time').keydown(function(e) {
	    if(e.keyCode == 13) {
	        $('#add').click();
	    }
	});
}

function clearInputs() {
	$('#hash').val('');
	$('#time').val('');
}

function updateHashtags() {
	$.ajax({
		url: 'Hash/getData',
		dataType: 'JSON',
		beforeSend: function() {
			tableContent.clear();
		},
		success: function(data) {
			$.each(data, function(key, val) {
				addRow(val);
			});
			updateBarChart(data);
		},
		error: function(err){
			errorMessage('Um erro inesperado aconteceu.');
		},
		complete: function() {
			tableContent.draw()
		}
	})
}

function initializeBarChart() {
    var ctx = $("#tweetsBar");

    tweetsBar = new Chart(ctx, {
        type: 'bar',
        options: {
            legend: {
                position: 'bottom'
            },
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero:true
                    }
                }]
            },
        },
    });
    // ctx.height($("#performedOSGroup").height());
}

function updateBarChart(data) {
    var datasets = [];
    var dataLabel = [];
    var values = []

    $.each(data, function(key, val) {
    	values.push(val.tweets_created);
	    dataLabel.push(val.hash);
    });


    datasets.push(
        generateDataset(values, chartColors.green,''),
    );
    tweetsBar.data.datasets = datasets;
    tweetsBar.data.labels = dataLabel;
    tweetsBar.update();
}


function addRow(obj) {
	//Stop button
	var button = '<input id="' + obj.id + '" type="button" class="btn btn-danger stop" value="Parar">';
	var duration = moment.duration(obj.duration, 'seconds').format({
			'template': 'hh:mm:ss',
			'trim' : false
		});
	var hourSufix = ' horas'
	if (obj.time == 1) {
		hourSufix = ' hora';
	}
	tableContent.row.add([
		'#'+obj.hash,
		obj.created_at,
		obj.time + hourSufix,
		duration ,
		obj.tweets_created,
		button,
	]);
}
